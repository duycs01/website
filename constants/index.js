export const constants = {
  navMenu:[
    {title: 'Trang chủ', href:'/'},
    {title: 'Giới thiệu', href:'/introduction'},
    {title: 'Dự án ',  href:'/project'},
    {title: 'Tin tức & Sự kiện', href:'/news'},
    {title: 'Thư viện', href:'/library'},
  ],
  newsCard:[
    {
      title:'Hoàn thành việc xông hệ thống điện đự án Golden Park Tower',
      image:require('@/assets/images/aboutus_image1.jpg'),
      date:'02/12/2020',
      time: '08:28 AM',
      content: 'Vào ngày 30 tháng 11 năm 2020. Công ty TNHH đầu tư xây dựng và xuất nhập khẩu Tây Đô chủ đầu tư dự án Golden Park Tower đã hoàn thành việc xông hệ thống điện cho dự án Golden Park. Đây là một trong những công việc để chuẩn bị tiến hành bàn giao căn hộ cho các khách hàng mua nhà tại dự án.',
      imageContent:[
        {
          image:require('@/assets/images/aboutus_details1.jpg'),
        },
        {
          image: require('@/assets/images/aboutus_details2.jpg'),
        }
      ],
      description:'Vào ngày 30 tháng 11 năm 2020. Công ty TNHH đầu tư xây dựng và xuất nhập khẩu Tây Đô chủ đầu tư dự án Golden Park Tower đã hoàn thành việc xông hệ thống điện cho dự án Golden Park',
    },
    {
      title:'Thông báo về việc phát hành sổ cổ đông của Công ty CP Đầu tư Phát triển nhà Constrexim năm 2020',
      image:require('@/assets/images/aboutus_image2.jpg'),
      date:'18/11/2020',
      time: '08:28 AM',
      content: 'Vào ngày 30 tháng 11 năm 2020. Công ty TNHH đầu tư xây dựng và xuất nhập khẩu Tây Đô chủ đầu tư dự án Golden Park Tower đã hoàn thành việc xông hệ thống điện cho dự án Golden Park. Đây là một trong những công việc để chuẩn bị tiến hành bàn giao căn hộ cho các khách hàng mua nhà tại dự án.',
      imageContent:[
        {
          image:require('@/assets/images/aboutus_details1.jpg'),
        },
        {
          image: require('@/assets/images/aboutus_details2.jpg'),
        }
      ],
      description:'Thông báo về việc phát hành sổ cổ đông của Công ty CP Đầu tư Phát triển nhà Constrexim năm 2020',
    },
    {
      title:'Thông báo kết quả đấu giá quyền sử dụng đất ở tại khu dân cư hồ Mật Sơn, TP Chí Linh, tỉnh Hải Dương',
      image:require('@/assets/images/aboutus_image3.jpg'),
      date:'21/10/2020',
      time: '08:28 AM',
      content: 'Vào ngày 30 tháng 11 năm 2020. Công ty TNHH đầu tư xây dựng và xuất nhập khẩu Tây Đô chủ đầu tư dự án Golden Park Tower đã hoàn thành việc xông hệ thống điện cho dự án Golden Park. Đây là một trong những công việc để chuẩn bị tiến hành bàn giao căn hộ cho các khách hàng mua nhà tại dự án.',
      imageContent:[
        {
          image:require('@/assets/images/aboutus_details1.jpg'),
        },
        {
          image: require('@/assets/images/aboutus_details2.jpg'),
        }
      ],
      description:'',
    },
    {
      title:'Công ty CONSTREXIM – HOD tổ chức kỷ niệm ngày phụ nữ Việt Nam 20/10/2020.',
      image:require('@/assets/images/aboutus_image4.jpg'),
      date:'20/10/202',
      time: '08:28 AM',
      content: 'Vào ngày 30 tháng 11 năm 2020. Công ty TNHH đầu tư xây dựng và xuất nhập khẩu Tây Đô chủ đầu tư dự án Golden Park Tower đã hoàn thành việc xông hệ thống điện cho dự án Golden Park. Đây là một trong những công việc để chuẩn bị tiến hành bàn giao căn hộ cho các khách hàng mua nhà tại dự án.',
      imageContent:[
        {
          image:require('@/assets/images/aboutus_details1.jpg'),
        },
        {
          image: require('@/assets/images/aboutus_details2.jpg'),
        }
      ],
      description:'Nhân kỷ niệm ngày 90 năm ngày phụ nữ Việt Nam (20-10-1930 – 20-10-2020). Ngày 17 tháng 10 năm 2020. Công ty và Công đoàn Công ty tổ chức chuyến tham quan cho CBNV trong toàn hệ thống Công ty CONSTREXIM – HOD tại quần thể danh thắng Tràng An, tỉnh Ninh Bình.',

    },
    {
      title:'Phát động thi đua trên công trường dự án Golden Park Tower',
      image:require('@/assets/images/aboutus_image5.jpg'),
      date:'20/10/2020',
      time: '08:28 AM',
      content: 'Vào ngày 30 tháng 11 năm 2020. Công ty TNHH đầu tư xây dựng và xuất nhập khẩu Tây Đô chủ đầu tư dự án Golden Park Tower đã hoàn thành việc xông hệ thống điện cho dự án Golden Park. Đây là một trong những công việc để chuẩn bị tiến hành bàn giao căn hộ cho các khách hàng mua nhà tại dự án.',
      imageContent:[
        {
          image:require('@/assets/images/aboutus_details1.jpg'),
        },
        {
          image: require('@/assets/images/aboutus_details2.jpg'),
        }
      ],
      description:'Sáng ngày 15/10/2020, tại công trình dự án Golden Park Tower, Công ty CP Đầu tư phát triển nhà Constrexim ( CONSTREXIM – HOD) tổ chức lễ phát động thi đua “45 ngày đêm về đích” hoàn thành tiến độ xây dựng công trình tổ hợp khách sạn, văn phòng và trung tâm thương mại Golden Park .',
    },
    {
      title:'THÔNG BÁO ĐẤU GIÁ',
      image:require('@/assets/images/aboutus_image6.jpg'),
      date:'01/10/2020',
      time: '08:28 AM',
      content: 'Vào ngày 30 tháng 11 năm 2020. Công ty TNHH đầu tư xây dựng và xuất nhập khẩu Tây Đô chủ đầu tư dự án Golden Park Tower đã hoàn thành việc xông hệ thống điện cho dự án Golden Park. Đây là một trong những công việc để chuẩn bị tiến hành bàn giao căn hộ cho các khách hàng mua nhà tại dự án.',
      imageContent:[
        {
          image:require('@/assets/images/aboutus_details1.jpg'),
        },
        {
          image: require('@/assets/images/aboutus_details2.jpg'),
        }
      ],
      description:'',
    },
  ],
  slider:[
    {image: require('@/assets/images/home_slide1.jpg')},
    {image: require('@/assets/images/home_slide2.jpg')},
    {image: require('@/assets/images/home_slide3.jpg')},
    {image: require('@/assets/images/home_slide4.jpg')},
    {image: require('@/assets/images/home_slide5.jpg')},
    {image: require('@/assets/images/home_slide6.jpg')},
  ],
  libraryImage:[
    {
      title: 'Abum ảnh năm 2020',
      image: require('@/assets/images/library_image1.jpg')
    },
    {
      title: 'Abum ảnh năm 2020',
      image: require('@/assets/images/library_image2.jpg')
    },
    {
      title: 'Abum ảnh năm 2020',
      image: require('@/assets/images/library_image3.jpg')
    },
    {
      title: 'Abum ảnh năm 2020',
      image: require('@/assets/images/library_image4.jpg')
    },
    {
      title: 'Abum ảnh năm 2020',
      image: require('@/assets/images/library_image5.jpg')
    },
  ],
  libraryVideo:[
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video1.jpg')
    },
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video2.jpg')
    },
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video3.jpg')
    },
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video4.jpg')
    },
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video5.jpg')
    },
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video6.jpg')
    },
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video7.jpg')
    },{
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video8.jpg')
    },
    {
      title: 'Lễ cất nóc dự án Golden Park Tower',
      video: require('@/assets/images/library_video9.jpg')
    },
  ],
  project:[
    {
      title: 'Dự án Golden Park Tower',
      image: require('@/assets/images/home_slide1.jpg'),
      address: 'Địa chỉ: Số 2 Phạm Văn Bạch, Yên Hòa, Cầu Giấy, Hà Nội'
    },
    {
      title: 'Dự án Golden Park Tower',
      image: require('@/assets/images/home_slide2.jpg'),
      address: 'Địa chỉ: Số 2 Phạm Văn Bạch, Yên Hòa, Cầu Giấy, Hà Nội'
    },
    {
      title: 'Dự án Golden Park Tower',
      image: require('@/assets/images/home_slide3.jpg'),
      address: 'Địa chỉ: Số 2 Phạm Văn Bạch, Yên Hòa, Cầu Giấy, Hà Nội'
    },
    {
      title: 'Dự án Golden Park Tower',
      image: require('@/assets/images/home_slide4.jpg'),
      address: 'Địa chỉ: Số 2 Phạm Văn Bạch, Yên Hòa, Cầu Giấy, Hà Nội'
    },
    {
      title: 'Dự án Golden Park Tower',
      image: require('@/assets/images/home_slide5.jpg'),
      address: 'Địa chỉ: Số 2 Phạm Văn Bạch, Yên Hòa, Cầu Giấy, Hà Nội'
    },
    {
      title: 'Dự án Golden Park Tower',
      image: require('@/assets/images/home_slide6.jpg'),
      address: 'Địa chỉ: Số 2 Phạm Văn Bạch, Yên Hòa, Cầu Giấy, Hà Nội'
    },
  ],
  historyIntroduction:[
    {
      year: '2010',
      month: 'Tháng 12',
      title: 'Ban Kinh doanh Phát triển nhà',
      description: '\n' +
        '        thuộc Tổng Công ty Đầu tư xây dựng và Thương mại Việt Nam\n' +
        '        (Constrexim Holding) - Bộ Xây dựng',
    },
    {
      year: '2015',
      month: 'Tháng 12',
      title: 'Ban Kinh doanh Phát triển nhà',
      description: '\n' +
        '        thuộc Tổng Công ty Đầu tư xây dựng và Thương mại Việt Nam\n' +
        '        (Constrexim Holding) - Bộ Xây dựng',
    },
    {
      year: '2017',
      month: 'Tháng 12',
      title: 'Ban Kinh doanh Phát triển nhà',
      description: '\n' +
        '        thuộc Tổng Công ty Đầu tư xây dựng và Thương mại Việt Nam\n' +
        '        (Constrexim Holding) - Bộ Xây dựng',
    },
    {
      year: '2020',
      month: 'Tháng 12',
      title: 'Ban Kinh doanh Phát triển nhà',
      description: '\n' +
        '        thuộc Tổng Công ty Đầu tư xây dựng và Thương mại Việt Nam\n' +
        '        (Constrexim Holding) - Bộ Xây dựng',
    }
  ],
  introductionPrize:[
    {image: require('@/assets/images/prize_2.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/prize_2.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/prize_2.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/prize_2.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/prize_1.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/prize_2.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/prize_3.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
  ],
  introductionCommunity:[
    {image: require('@/assets/images/community_1.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/community_2.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/community_3.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/community_3.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/community_3.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
    {image: require('@/assets/images/community_3.jpg'), title:"Thăm hỏi gia đình CBCNV trong công ty nhân dịp Tết cổ truyền năm 2019 "},
  ]
}
