export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    title: 'Hoa Binh',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Hoa Binh' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/facvicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' },

      // {src:'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css', type:'text/css' , rel: 'stylesheet'},
      // {src:'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css', type:'text/css', rel: 'stylesheet',},
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@/assets/css/reset.css',
    '@/assets/css/responsive.css',
    '@/assets/css/style.css',
    '@/assets/css/slider.css',
  ],
  styleResource: {
    scss: [
      '@/assets/css/styleScss.scss',
    ],
  },
  script:[
    {src:'https://code.jquery.com/jquery-3.2.1.slim.min.js', type:'text/javascript'},
    {src:'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', type:'text/javascript'},
    {src:'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', type:'text/javascript'},
    // {src:'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js', type:'text/javascript'},
    // {src:"@/assets/js/script.js", type:'text/javascript'}
  ],
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {

  }
}
